/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  ngrajales
 * Created: 11/05/2017
 */

-- Sequence: public.seq_id_administradora

-- DROP SEQUENCE public.seq_id_administradora;

CREATE SEQUENCE public.seq_id_administradora
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE public.seq_id_administradora
  OWNER TO postgres;


-- Table: public.administrador

-- DROP TABLE public.administrador;

CREATE TABLE public.administrador
(
  id integer NOT NULL,
  codigo character varying(20),
  nombre character varying(200),
  cod_tp_id character(2),
  nro_id character varying(50),
  naturaleza character(2),
  multiple_arp integer,
  fsp integer,
  fusionada integer,
  fecha_fusion date,
  CONSTRAINT pk_administrador PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.administrador
  OWNER TO postgres;
