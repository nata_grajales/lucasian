/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.co.lucasian.bean;

import com.co.lucasian.entidad.Administrador;
import com.co.lucasian.facade.AdministradorFacade;
import com.co.lucasian.util.FacesUtils;
import com.co.lucasian.util.JsfUtil;
import com.co.lucasian.util.Utilidades;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author ngrajales
 */
@Named(value = "cargarDocBean")
@SessionScoped
public class CargarDocBean implements Serializable {

    private UploadedFile doc_carga;
    private String ubicacion;
    private Boolean cargarDoc = null;
    private StringBuilder error;
    private List<Administrador> listGuardar;
    @EJB
    private AdministradorFacade administradorFacade;

    /**
     * Creates a new instance of CargarDocBean
     */
    public CargarDocBean() {
    }

    /**
     * Metodo Llamado desde la vista
     */
    public void cargarDocumento() {
        try {
            validarDocumento(guardarDoc(getDoc_carga().getFileName(), getDoc_carga().getInputstream()));
        } catch (IOException e) {
            e.printStackTrace();
            JsfUtil.addErrorMessage(ResourceBundle.getBundle("/Bundle").getString("error_cargar_doc"));
        }

    }

    /**
     * Metodo Encargado guardar el documento en la carpeta temporal del proyecto
     *
     * @param fileName
     * @param in
     */
    private String guardarDoc(String fileName, InputStream in) {
        try {
            ubicacion = FacesUtils.getPath() + "documentos/" + fileName;
            OutputStream out = new FileOutputStream(new File(ubicacion));
            int read = 0;
            byte[] bytes = new byte[1024];

            while ((read = in.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
            in.close();
            out.flush();
            out.close();
            return ubicacion;
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return null;

    }

    /**
     * Encargada de Leer  y guardar el documento
     * @param ruta 
     */
    public void validarDocumento(String ruta) {
        File f = new File(ruta);
        BufferedReader entrada;
        try {
            entrada = new BufferedReader(new FileReader(f));

            error = new StringBuilder();
            int numeroLinea = 0;
            listGuardar = new ArrayList<>();
            cargarDoc = true;

            while (entrada.ready()) {
                numeroLinea++;
                String[] doc = entrada.readLine().toUpperCase().split(";");
                if (doc.length != 9) {
                    error.append("Linea ").append(numeroLinea).append(" Incompleta \n");
                } else {
                    validarLinea(doc, numeroLinea);
                }
            }
            if (cargarDoc) {
                listGuardar.forEach((administrador) -> {
                    administradorFacade.create(administrador);
                });
                JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("exito"));

            } else {
                JsfUtil.addErrorMessage(ResourceBundle.getBundle("/Bundle").getString("error_guardar_doc"));
                JsfUtil.addErrorMessage(error.toString());
            }
        } catch (IOException e) {
            System.out.println("e = " + e);
        }
    }

    /**
     * Encargado de realizar la validación de cada uno de los campos
     * @param str
     * @param linea
     * @return 
     */
    public Boolean validarLinea(String[] str, int linea) {
        Administrador admin;
        admin = new Administrador();
        error.append("\n\n Linea ").append(linea).append("\n");
        if (Utilidades.isNumeroLetra(str[0])) {
            admin.setCodigo(str[0]);
        } else {
            cargarDoc = false;
            error.append("El codigo Tiene Alfanumericos ").append(str[0]).append(",");
        }
        if (Utilidades.isNumeroLetra(str[1])) {
            admin.setNombre(str[1]);
        } else {
            cargarDoc = false;
            error.append("El nombre Tiene Alfanumericos ").append(str[1]).append(",");
        }
        if (Utilidades.isTipoIdentificacion(str[2])) {
            admin.setCodTpId(str[2]);
        } else {
            cargarDoc = false;
            error.append("No es un tipo de identificación ").append(str[2]).append(",");
        }
        if (Utilidades.isNumero(str[3])) {
            admin.setNroId(str[3]);
        } else {
            cargarDoc = false;
            error.append("No es un correcta la identificación ").append(str[3]).append(",");
        }
        if (Utilidades.isTipoNaturaleza(str[4])) {
            admin.setNaturaleza(str[4]);
        } else {
            cargarDoc = false;
            error.append("No es un correcta la Naturaleza ").append(str[4]).append(",");
        }
        if (Utilidades.isXONull(str[5])) {
            if (str[5].equals("X")) {
                admin.setMultipleArp(1);
            } else {
                admin.setMultipleArp(0);
            }
        } else {
            cargarDoc = false;
            error.append("No es un correcta la Multiple Arp ").append(str[5]).append(",");
        }

        if (Utilidades.isXONull(str[6])) {
            if (str[6].equals("X")) {
                admin.setFsp(1);
            } else {
                admin.setFsp(0);
            }
        } else {
            cargarDoc = false;
            error.append("No es un correcta la FSP  ").append(str[6]).append(",");
        }

        if (Utilidades.isXONull(str[7])) {
            if (str[7].equals("X")) {
                admin.setFusionada(1);
            } else {
                admin.setFusionada(0);
            }
        } else {
            cargarDoc = false;
            error.append("No es un correcta la Naturaleza ").append(str[7]).append(",");
        }
        if (Utilidades.isFechaValida(str[8]) && admin.getFusionada() == 1) {
            admin.setFechaFusion(new Date(str[8]));
        } else {
            if (admin.getFusionada() == 1) {
                cargarDoc = false;
                error.append("No es un correcta la Fecha de funcionalidad ").append(str[8]).append(",");
            }
        }
        listGuardar.add(admin);
        return cargarDoc;

    }

    public UploadedFile getDoc_carga() {
        return doc_carga;
    }

    public void setDoc_carga(UploadedFile doc_carga) {
        this.doc_carga = doc_carga;
    }

    public Boolean getCargarDoc() {
        return cargarDoc;
    }

    public StringBuilder getError() {
        return error;
    }

    
    
    

}
