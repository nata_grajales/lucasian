/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.co.lucasian.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;
/**
 *
 * @author ngrajales
 */
public class Utilidades {

    public static boolean isNumero(String str) {
        return str.matches("-?\\d+(\\.\\d+)?");
    }


    public static boolean isNumeroLetra(String str) {
        return str.matches("[A-Za-z1-9]+");
    }

    public static boolean isTipoIdentificacion(String str) {
        return str.equals("NI") || str.equals("CC") || str.equals("PA") || str.equals("RC");
    }

    public static boolean isTipoNaturaleza(String str) {
        return str.equals("PR") || str.equals("PU") || str.equals("MI");
    }

    public static boolean isXONull(String str) {
        return str.equals("X") || str.equals("NULL") || str.equals("");
    }

    public static boolean isFechaValida(String fecha) {
        try {
            SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
            formatoFecha.setLenient(false);
            formatoFecha.parse(fecha);
        } catch (ParseException e) {
            return false;
        }
        return true;
    }

}
