/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.co.lucasian.entidad;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ngrajales
 */
@Entity
@Table(name = "administrador")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Administrador.findAll", query = "SELECT a FROM Administrador a")
    , @NamedQuery(name = "Administrador.findById", query = "SELECT a FROM Administrador a WHERE a.id = :id")
    , @NamedQuery(name = "Administrador.findByCodigo", query = "SELECT a FROM Administrador a WHERE a.codigo = :codigo")
    , @NamedQuery(name = "Administrador.findByNombre", query = "SELECT a FROM Administrador a WHERE a.nombre = :nombre")
    , @NamedQuery(name = "Administrador.findByCodTpId", query = "SELECT a FROM Administrador a WHERE a.codTpId = :codTpId")
    , @NamedQuery(name = "Administrador.findByNroId", query = "SELECT a FROM Administrador a WHERE a.nroId = :nroId")
    , @NamedQuery(name = "Administrador.findByNaturaleza", query = "SELECT a FROM Administrador a WHERE a.naturaleza = :naturaleza")
    , @NamedQuery(name = "Administrador.findByMultipleArp", query = "SELECT a FROM Administrador a WHERE a.multipleArp = :multipleArp")
    , @NamedQuery(name = "Administrador.findByFsp", query = "SELECT a FROM Administrador a WHERE a.fsp = :fsp")
    , @NamedQuery(name = "Administrador.findByFusionada", query = "SELECT a FROM Administrador a WHERE a.fusionada = :fusionada")
    , @NamedQuery(name = "Administrador.findByFechaFusion", query = "SELECT a FROM Administrador a WHERE a.fechaFusion = :fechaFusion")})
public class Administrador implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @SequenceGenerator(name = "seq_id_administradora", sequenceName = "seq_id_administradora", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_id_administradora")
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    @Size(max = 20)
    @Column(name = "codigo")
    private String codigo;
    @Size(max = 200)
    @Column(name = "nombre")
    private String nombre;
    @Size(max = 2)
    @Column(name = "cod_tp_id")
    private String codTpId;
    @Size(max = 50)
    @Column(name = "nro_id")
    private String nroId;
    @Size(max = 2)
    @Column(name = "naturaleza")
    private String naturaleza;
    @Column(name = "multiple_arp")
    private Integer multipleArp;
    @Column(name = "fsp")
    private Integer fsp;
    @Column(name = "fusionada")
    private Integer fusionada;
    @Column(name = "fecha_fusion")
    @Temporal(TemporalType.DATE)
    private Date fechaFusion;

    public Administrador() {
    }

    public Administrador(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCodTpId() {
        return codTpId;
    }

    public void setCodTpId(String codTpId) {
        this.codTpId = codTpId;
    }

    public String getNroId() {
        return nroId;
    }

    public void setNroId(String nroId) {
        this.nroId = nroId;
    }

    public String getNaturaleza() {
        return naturaleza;
    }

    public void setNaturaleza(String naturaleza) {
        this.naturaleza = naturaleza;
    }

    public Integer getMultipleArp() {
        return multipleArp;
    }

    public void setMultipleArp(Integer multipleArp) {
        this.multipleArp = multipleArp;
    }

    public Integer getFsp() {
        return fsp;
    }

    public void setFsp(Integer fsp) {
        this.fsp = fsp;
    }

    public Integer getFusionada() {
        return fusionada;
    }

    public void setFusionada(Integer fusionada) {
        this.fusionada = fusionada;
    }

    public Date getFechaFusion() {
        return fechaFusion;
    }

    public void setFechaFusion(Date fechaFusion) {
        this.fechaFusion = fechaFusion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Administrador)) {
            return false;
        }
        Administrador other = (Administrador) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.co.lucasian.entidad.Administrador[ id=" + id + " ]";
    }

}
